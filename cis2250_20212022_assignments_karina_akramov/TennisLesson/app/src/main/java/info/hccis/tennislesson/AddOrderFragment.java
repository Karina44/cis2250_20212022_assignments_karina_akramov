package info.hccis.tennislesson;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import info.hccis.tennislesson.BO.OrderViewModel;
import info.hccis.tennislesson.databinding.FragmentAddOrderBinding;
import info.hccis.tennislesson.BO.Order;


/*
 *Author: Karina Akramov
 * Date: 2022-01-19
 * Allows user to enter the order details
 * */
public class AddOrderFragment extends Fragment {

    public static final String KEY = "info.hccis.performancehall.ORDER";
    private FragmentAddOrderBinding binding;
    Order orderbo;

    OrderViewModel orderViewModelbo;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentAddOrderBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d("AddOrderFragment", "onViewCreated triggered");

        orderViewModelbo = new ViewModelProvider(getActivity()).get(OrderViewModel.class);

        binding.buttonCalculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("AddOrderFragment", "Calculate was clicked");

                try {
                    calculate();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(KEY, orderbo);


                    NavHostFragment.findNavController(AddOrderFragment.this)
                            .navigate(R.id.action_AddOrderFragment_to_ViewOrderFragment, bundle);
                } catch (Exception e) {
                    Log.d("AddOrderFragment", "Error calculating: " + e.getMessage());
                }
            }
        });

    }


    /**
     * calculate the ticket cost based on the controls on the view.
     *
     * @throws Exception Throw exception if number of tickets entered caused an issue.
     * @author CIS2250
     * @since 20220118
     */
    public void calculate() throws Exception {

        Log.d("MainActivity", "Is a Member?=" + binding.editTextIsMember.getText().toString());
        Log.d("MainActivity", "Number of People = " + binding.editTextNumberOfPeople.getText().toString());
        Log.d("MainActivity", "Calculate button was clicked.");


        String isMember = binding.editTextIsMember.getText().toString();


        int numberOfPeople;
        int numberOfHours;
        try {
            numberOfPeople = Integer.parseInt(binding.editTextNumberOfPeople.getText().toString());
            numberOfHours = Integer.parseInt(binding.editTextNumberOfHours.getText().toString());
        } catch (Exception e) {
            numberOfPeople = 0;
            numberOfHours = 0;
        }
        orderbo = new Order(isMember, numberOfPeople, numberOfHours);

        try {

            double cost = orderbo.calculate();
            Log.d("MainActivity", "cost=" + cost);
            OrderViewModel.getOrders().add(orderbo);

            //Now that I have the cost, want to set the value on the textview.
            Locale locale = new Locale("en", "CA");
            DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
            DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(locale);
            decimalFormat.setDecimalFormatSymbols(dfs);
            String formattedCost = decimalFormat.format(cost);
            //  binding.textViewCost.setText(formattedCost);

        } catch (NumberFormatException nfe) {
            binding.editTextNumberOfPeople.setText("");
            //  binding.textViewCost.setText("Invalid number of tickets");
            throw nfe;
        } catch (Exception e) {
            binding.editTextNumberOfPeople.setText("");
            //  binding.textViewCost.setText("Maximum number of tickets is "+TicketOrderBO.MAX_TICKETS);
            throw e;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}