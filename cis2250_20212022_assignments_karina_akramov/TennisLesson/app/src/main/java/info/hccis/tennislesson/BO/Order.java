package info.hccis.tennislesson.BO;

import java.io.Serializable;

/*
 *Author: Badalkumar patel
 * Date: 21st January 2022
 * subject: This is the Order class used for the business object
 * */
public class Order implements Serializable {
    public static final double memberPrivateRate = 55;
    public static final double memberTwoPeopleRate = 30;
    public static final double memberThreePeopleRate = 21;
    public static final double memberFourPeopleRate = 16;
    public static final double nonMemberPrivateRate = 60;
    public static final double nonMemberTwoPeopleRate = 33;
    public static final double nonMemberThreePeopleRate = 23;
    public static final double nonMemberFourPeopleRate = 18;

    private String isMember;
    private int groupSize;
    private double numberOfHours;
    private double total;

    public Order(String Member, int numberOfPeople, int hours) {

        this.isMember = Member;
        this.groupSize = numberOfPeople;
        this.numberOfHours = hours;

    }

    public double calculate() {

        switch (isMember) {
            case "y":
            case "Y":
                if (groupSize == 1) {
                    total = numberOfHours * memberPrivateRate;
                } else if (groupSize == 2) {
                    total = numberOfHours * memberTwoPeopleRate;
                } else if (groupSize == 3) {
                    total = numberOfHours * memberThreePeopleRate;
                } else if (groupSize == 4) {
                    total = numberOfHours * memberFourPeopleRate;
                }
                break;
            case "n":
            case "N":
                if (groupSize == 1) {
                    total = numberOfHours * nonMemberPrivateRate;
                } else if (groupSize == 2) {
                    total = numberOfHours * nonMemberTwoPeopleRate;
                } else if (groupSize == 3) {
                    total = numberOfHours * nonMemberThreePeopleRate;
                } else if (groupSize == 4) {
                    total = numberOfHours * nonMemberFourPeopleRate;
                }
                break;
        }
        return total;
    }

    @Override
    public String toString() {
        return "Tennis Lesson Order" + System.lineSeparator()
                + "Is A Member=" + isMember +
                ", Number or Hours=" + numberOfHours +
                ", Number of People=" + groupSize + System.lineSeparator()
                + "Order cost: $" + calculate();
    }
}
