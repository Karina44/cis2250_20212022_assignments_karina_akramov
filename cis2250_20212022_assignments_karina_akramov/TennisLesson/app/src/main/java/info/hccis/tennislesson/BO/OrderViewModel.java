package info.hccis.tennislesson.BO;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

/*
 *Author: Badalkumar patel
 * Date: 21st January 2022
 * subject: This is the View order model for the user to have all order history
 * */
public class OrderViewModel extends ViewModel {

    private static ArrayList<Order> Orders = new ArrayList();

    public static ArrayList<Order> getOrders() {
        return Orders;
    }

    public void setOrders(ArrayList<Order> Orders) {
        this.Orders = Orders;
    }
}
