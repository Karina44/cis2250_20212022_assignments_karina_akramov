package info.hccis.tennislesson;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import info.hccis.tennislesson.BO.Order;
import info.hccis.tennislesson.BO.OrderViewModel;
import info.hccis.tennislesson.databinding.FragmentViewOrderBinding;


/*
 *Author: Karina Akramov
 * Date: 2022-01-19
 * Class to view history of all orders
 * */
public class ViewOrderFragment extends Fragment {

    private FragmentViewOrderBinding binding;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        binding = FragmentViewOrderBinding.inflate(inflater, container, false);
        return binding.getRoot();

    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        OrderViewModel ticketOrderViewModel = new ViewModelProvider(getActivity()).get(OrderViewModel.class);

        //Bundle is accessed to get the ticket order which is passed from the add order fragment.
        Bundle bundle = getArguments();
        Order orderBo = (Order) bundle.getSerializable(AddOrderFragment.KEY);
        Log.d("ViewOrdersFragment Bada", "Ticket passed in:  " + orderBo.toString());


        String output = "";
        double total = 0;
        for (Order order : ticketOrderViewModel.getOrders()) {
            output += order.toString() + System.lineSeparator();
            total += order.calculate();
        }
        output += System.lineSeparator() + "Total: $" + total;

        binding.textviewSummary.setText(output);


        //Button sends the user back to the add fragment
        binding.buttonAddOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ViewOrderFragment.this)
                        .navigate(R.id.action_ViewOrderFragment_to_AddOrderFragment);
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}